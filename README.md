# fedora-install

A basic get-up-and-running Fedora install script including various customizations.

Forked from https://git.furworks.de/tobias/fedora-install.

<!-- FIXME This does not work anymore

## Things YOU have to do after:
Read the script! Seriously!

There are additions, like if you have an AMD gpu and want to use the much better mesa-aco to render things you need to run steamlike this in a terminal/override then flatpak env:

``` bash
FLATPAK_GL_DRIVERS=mesa-aco flatpak run com.valvesoftware.Steam
```

Or making it permanent for amd so you only need to click the icon:
``` bash
flatpak override --env=FLATPAK_GL_DRIVERS=mesa-aco --user
```

Same goes for allowing access to external harddrives/controllers for it
``` bash
# Allow talking to controllers
flatpak override --user --device=all com.valvesoftware.Steam
# Allow access to /put-your-own-path-here
flatpak override --filesystem=/put-your-own-path-here com.valvesoftware.Steam
```

Or setting a powersave profile for tuned:
``` bash
sudo tuned-adm profile powersave
```

Linux is a journey, this is only the beginning. There's always more to learn if you want to!
-->

## RUN THIS AS YOUR USER!

Seriously, this changes usersettings. root for the entire script will NOT work.
It may require you due to the time it takes to enter your password multiple times.

There are multiple flags that the install script can use:
* `--nonfree`: Nonfree Additions will be added
* `--steam`: Adding Steam as flatpak to avoid fedora lib misaligment issues for games
* `--laptop`: Adding Laptop configurations like power saving features etc.
* `--clamav`: Install and configure the virus scanner ClamAV (mostly needed to protect Windows if used as a secondary OS)
* `--thinkpad`: Adding Thinkpad-specific configurations (currently only concerning additional power saving features)

## For a free, open source only install run it using

```bash
#installs wget, gets the script, makes it executable and runs it
sudo dnf install -y wget && wget "https://gitlab.com/FKHals/fedora-install/-/raw/master/install.sh" -O ./install.sh && chmod +x ./install.sh && ./install.sh
```

## To also get nonfree extensions, like rpmfusion nonfree upstream or steam flatpak run it this way instead

```bash
#installs wget, gets the script, makes it executable and runs it with steam and nonfree repos added for things like nvidia drivers
sudo dnf install -y wget && wget "https://gitlab.com/FKHals/fedora-install/-/raw/master/install.sh" -O ./install.sh && chmod +x ./install.sh && ./install.sh --nonfree --steam
```
