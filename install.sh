#!/bin/bash
#########################################################################################
# Fedora (fresh) install script
#
# This script installs all (for the author) necessary programs and configurations on a
# System running Fedora (Linux OS).
#
# Usage: `bash <scriptname>` or `./<scriptname>`
# (instead of `. <scriptname>` because otherwise the "strict"-mode would exit the script
# AND the terminal immediately on error)
#
# by Felix Korthals
# October 2020
#########################################################################################

# TODO This is not actually idempotent since it appends the string line-wise which can cause a line to miss when the same line is already contained at another place in the script (which would be undesirable e.g. for conditionals)
# appends string (line by line) to a file if it the corresponding line have not already been appended 
# ${1}: Filename to append to
# ${2}: String to append
# (idea: https://arslan.io/2019/07/03/how-to-write-idempotent-bash-scripts/)
su_idempotent_file_append() {
filename=${1}
# skip the first argument (which contains the filename) and iterate of the
# lines of the string
for line in ${@:2}
do
    # F - for fixed-string match (no regexes)
    # q - suppress output of matched lines
    # s - suppress if the file does not exists since then it will be created in the next step
    # x - for full line match
    # (source: https://unix.stackexchange.com/questions/416786/how-to-append-multiple-lines-to-a-file-if-these-lines-doesnt-exist-in-that-fil/417797#417797)
    if ! grep -Fqsxe "$line" "$filename" ; then
        sudo tee --append ${1} > /dev/null  <<EOF
$line
EOF
    fi
done
}

# TODO Deduplicate with the above function (su_idempotent_file_append)
# non-sudo variant of the above
idempotent_file_append() {
filename=${1}
for line in ${@:2}
do
    if ! grep -Fqsxe "$line" "$filename" ; then
        tee --append ${1} > /dev/null  <<EOF
$line
EOF
    fi
done
}

# use an unofficial "strict-mode" so the script won't fail silently
set -uo pipefail
trap 's=$?; echo "$0: Error on line "$LINENO": $BASH_COMMAND"; exit $s' ERR
# set Internal Field Separtor to make word splittings only happen on newlines or tabs
# because the (normally present) whitespaces are "likely to cause surprising and
# confusing bugs" (see http://redsymbol.net/articles/unofficial-bash-strict-mode/)
IFS=$'\n\t'


if [ $(id -u) = 0 ]; then
   echo "This script changes your users gsettings and should thus not be run as root!"
   echo "You may need to enter your password multiple times!"
   exit 1
fi

NONFREE=false
STEAMFLAT=false
NOSYSTEMD=false
LAPTOP=false
THINKPAD=false
CLAMAV=false

while test $# -gt 0
do
    case "$1" in
        --nonfree) 
			echo "Nonfree Additions will be added"
			NONFREE=true
            ;;
        --steam) 
			echo "Adding Steam as flatpak to avoid fedora lib misaligment issues for games"
			STEAMFLAT=true
            ;;
        # This flag is needed for example for a CI/CD pipeline since Docker
        # images do not use systemd (and especially do not start it as init
        # system with PID 1)
        --nosystemd)
            echo "Skipping systemd-commands for systems that do not support it"
            NOSYSTEMD=true
            ;;
        # This adds power saving and other features relevant for laptops
        --laptop)
            echo "Adding Laptop configurations"
            LAPTOP=true
            ;;
        # This antivirus software is mostly needed in combination with Windows as a second OS on
        # the same system
        --clamav)
            echo "ClamAV (virus scanning tool) will be installed and configured"
            CLAMAV=true
            ;;
        # If it is a Thinkpad it must also be a laptop so make sure it is flagged as such
        --thinkpad)
            echo "Adding Thinkpad-specific configurations"
            THINKPAD=true
            LAPTOP=true
            ;;
    esac
    shift
done

# Some Kernel/Usability Improvements
t="
# to prevent https://stackoverflow.com/questions/47075661/error-user-limit-of-inotify-watches-reached-extreact-build
fs.inotify.max_user_watches=524288

# disable IPv4 port forwarding (for security reasons)
net.ipv4.ip_forward=0

# enable ECN (increases the likelihood of a TCP connection reacting quickly after a link outage)
net.ipv4.tcp_ecn=1

# # enable TCP BBR to improve network speed
net.core.default_qdisc=fq
net.ipv4.tcp_congestion_control=bbr

# decrease linux swappiness to minimum (better performance with enough RAM)
vm.swappiness=1
"
su_idempotent_file_append /etc/sysctl.conf "$t"


# DNF tweaks
# fastestmirror: Selects fastest mirror server for DNF
# deltarpm: Download only the changed files on RPM update
# max_parallel_downloads: Select maximum number of concurrent downloads
t="fastestmirror=true
deltarpm=true
max_parallel_downloads=6
"
su_idempotent_file_append /etc/dnf/dnf.conf "$t"


###
# Optionally clean all dnf temporary files
###

sudo dnf clean all

###
# RpmFusion Free Repo
# This is holding only open source, vetted applications - fedora just cant legally distribute them themselves thanks to 
# Software patents
###

sudo dnf install -y https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm

###
# RpmFusion NonFree Repo
# This includes Nvidia Drivers and more
###

if [ "$NONFREE" = true ]; then
    sudo dnf install -y https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
fi

###
# Disable the Modular Repos
# Given the added load at updates, and the issues to keep modules updated, in check and listed from the awful cli for it - remove entirely.
###

sudo sed -i 's/enabled=1/enabled=0/g' /etc/yum.repos.d/fedora-updates-modular.repo
sudo sed -i 's/enabled=1/enabled=0/g' /etc/yum.repos.d/fedora-modular.repo

# Testing Repos should be disabled anyways
sudo sed -i 's/enabled=1/enabled=0/g' /etc/yum.repos.d/fedora-updates-testing-modular.repo
sudo sed -i 's/enabled=1/enabled=0/g' /etc/yum.repos.d/rpmfusion-free-updates-testing.repo

# Rpmfusion makes this obsolete
sudo sed -i 's/enabled=1/enabled=0/g' /etc/yum.repos.d/fedora-cisco-openh264.repo

# Disable Machine Counting for all repos
sudo sed -i 's/countme=1/countme=0/g' /etc/yum.repos.d/*

# vscodium, an open source fork of vscode
## Add the GPG key of the repository:
sudo rpm --import https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/master/pub.gpg
## Add the repository:
t="
[gitlab.com_paulcarroty_vscodium_repo]
name=gitlab.com_paulcarroty_vscodium_repo
baseurl=https://paulcarroty.gitlab.io/vscodium-deb-rpm-repo/rpms/
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/master/pub.gpg
"
su_idempotent_file_append /etc/yum.repos.d/vscodium.repo "$t"

###
# Force update the whole system to the latest and greatest
###

sudo dnf upgrade --best --allowerasing --refresh -y

# And also remove any packages without a source backing them
sudo dnf distro-sync -y

###
# Install base packages and applications
###

sudo dnf install \
-y \
android-tools \
arc-theme `#A more comfortable GTK/Gnome-Shell Theme` \
hunspell-de `#Adds ability for system level spell checking to e.g. Gedit` \
breeze-cursor-theme `#A more comfortable Cursor Theme from KDE` \
calibre `# An eBook viewer/manager` \
chromium-freeworld `#Comes with hardware acceleration and all Codecs` \
codium `#A nice editor`\
colordiff `# diff, just colored like a git diff`\
cronie `# provides functions used to configure cronjobs like crontab` \
dnf-automatic `#Automatic system updates` \
eog `# The official Gnome image viewer Eye of Gnome` \
exfat-utils `#Allows managing exfat (android sd cards and co)` \
ffmpeg `#Adds Codec Support to Firefox, and in general` \
file-roller `#Archive manager` \
file-roller-nautilus `#More Archives supported in nautilus` \
fuse-exfat `#Allows mounting exfat` \
fuse-sshfs `#Allows mounting servers via sshfs` \
gcc-gfortran \
gdbm \
gedit `#A simple graphical text editor`\
gedit-plugins `#Plugins for gedit like git, column edit, etc.`\
ghc-devel \
gimagereader-gtk `#An OCR (Image to text) tool` \
gimp `#The Image Editing Powerhouse - and its plugins` \
git `#VCS done right` \
gnome-extensions-app `#Manage gnome extensions` \
gnome-shell-extension-user-theme `#Enables theming the gnome shell` \
gnome-tweaks `#Your central place to make gnome like you want` \
gparted \
gtkhash-nautilus `#To get a file hash via gui` \
gvfs-fuse `#gnome<>fuse` \
gvfs-mtp `#gnome<>android` \
gvfs-nfs `#gnome<>ntfs` \
gvfs-smb `#gnome<>samba` \
htop `#Cli process monitor` \
inkscape  `#Working with .svg files` \
java-1.8.0-openjdk \
java-1.8.0-openjdk-devel \
keepassxc  `#Password Manager` \
libreoffice-langpack-de `# German language pack for spell-checking in libreoffice` \
lm_sensors `#Show your systems Temparature` \
lshw \
make \
'mozilla-fira-*' `#A nice font family` \
nautilus `#graphical file manager` \
nautilus-extensions `#What it says on the tin` \
gnome-terminal-nautilus `# Open a terminal from nautilus` \
nextcloud-client-nautilus `# Nautilus Nextcloud integration` \
NetworkManager-openvpn-gnome `#To enforce that its possible to import .ovpn files in the settings` \
openmpi openmpi-devel `#To be able to compile MPI programs` \
p7zip `#Archives` \
p7zip-plugins `#Even more of them` \
pandoc \
papirus-icon-theme `#A quite nice icon theme` \
par2cmdline `#Parchive implementation for data integrity checks and repairs` \
pdfgrep \
perl-Image-ExifTool \
podman `#A Docker alternative` \
podman-docker `# Emulate Docker CLI using podman` \
powertop `#power usage cli monitor` \
pipewire-pulseaudio `# pulseaudio replacement` \
pv `#pipe viewer - see what happens between the | with output | pv | receiver ` \
python3-devel `#Python Development Gear` \
smartmontools `#Monitoring of e.g. disk degradation using SMART` \
telegram-desktop `#Chatting, with newer openssl and qt base!` \
texlive \
texlive-amscls \
texlive-businesscard-qrcode \
texlive-collection-langgerman \
texlive-collection-latexextra \
texlive-collection-xetex \
texlive-colorprofiles \
texlive-dehyph \
texlive-fancyhdr \
texlive-german \
texlive-hyphenex \
texlive-l3backend \
texlive-latex-base-dev \
texlive-latex-bin-dev \
texlive-latex-graphics-dev \
texlive-quran-de \
texlive-ragged2e \
texlive-svg \
texlive-titlesec \
texlive-xetex \
texlive-physics \
tilix `#The best terminal manager i know of` \
tilix-nautilus `#Adds right click open in tilix to nautilus` \
transmission `#Torrent Client` \
tuned `#Tuned can optimize your performance according to metrics. tuned-adm profile powersave can help you on laptops, alot` \
unar `#free rar decompression` \
unzip \
wavemon `#a cli wifi status tool` \
youtube-dl `#Allows you to download and save youtube videos but also to open their links by dragging them into mpv!` \
borgbackup `#If you need backups, this is your tool for it` \
iotop  `#disk usage cli monitor` \
meld `#Quick Diff Tool` \
nano `#Because pressing i is too hard sometimes` \
neovim `#the better vim` \
nethogs `#Whats using all your traffic? Now you know!` \
nload `#Network Load Monitor` \
tar \
valgrind \
vim-enhanced `#full vim` \
vlc \
wget \
wine \
torbrowser-launcher
#latexila `#previously gnome-latex` \ FIXME

# install zoom (for work purposes)
#wget https://zoom.us/client/latest/zoom_x86_64.rpm
#sudo dnf localinstall -y zoom_x86_64.rpm
#rm zoom_x86_64.rpm

# install "GNOME Shell Extension Installer" (because this is better than
# installing GNOME extensions using the internet browser) if not already
# installed
if [ ! -f /usr/bin/gnome-shell-extension-installer ]; then
    wget -O gnome-shell-extension-installer "https://github.com/brunelli/gnome-shell-extension-installer/raw/master/gnome-shell-extension-installer"
    chmod +x gnome-shell-extension-installer
    sudo mv gnome-shell-extension-installer /usr/bin/
fi
# The corresponding extension website:
# https://extensions.gnome.org/extension/<ID> (replace <ID>)
# For later activation investigate with `gsettings get org.gnome.shell enabled-extensions`

# An arraz of strings of enabled shell extensions (which is later used by gsettings)
ENABLED_SHELL_EXTENSIONS_ARRAY=()

# To find the names of already installed extensions use:
# gsettings get org.gnome.shell enabled-extensions

# "Sound Input & Output Device Chooser" for fast audio input/output switching
gnome-shell-extension-installer 906 --yes  # --yes skips all prompts (should install latest)
ENABLED_SHELL_EXTENSIONS_ARRAY+=('sound-output-device-chooser@kgshank.net')

# "Dark Theme Toggle for adwaita theme and papirus icons" to toggle between
# light and dark gnome themes
gnome-shell-extension-installer 2326 --yes
ENABLED_SHELL_EXTENSIONS_ARRAY+=('adwaita_papirus_dark-light_toggle@OldKnitterhemd')

# "NASA APOD Wallpaper Changer" to change your wallpaper daily to the NASA's
# astronomy picture of the day
# Alternatives: * 2290 - "Wikimedia Commons Wallpaper Changer" (Pic of the day)
#               * 1262 - "Bing Wallpaper" (analog zum Windows 10 wallpaper)
gnome-shell-extension-installer 1202 --yes
ENABLED_SHELL_EXTENSIONS_ARRAY+=('nasa_apod@elinvention.ovh')

# "Tweaks & Extensions in System Menu"
gnome-shell-extension-installer 1653 --yes
ENABLED_SHELL_EXTENSIONS_ARRAY+=('tweaks-system-menu@extensions.gnome-shell.fifi.org')

# "Resource Monitor"
# (do not install on laptops due to possible higher power draw due to more context switches)
if [ "$LAPTOP" = true ]; then
    gnome-shell-extension-installer 1634 --yes
    ENABLED_SHELL_EXTENSIONS_ARRAY+=('Resource_Monitor@Ory0n')
fi

# build the string of all enabled shell extensions
ENABLED_SHELL_EXTENSIONS="["
# append the first element outside the loop to be able to drop the comma so
# that the other elements can prepend it so that no trailing comma exists in
# the final string
ENABLED_SHELL_EXTENSIONS+="'${ENABLED_SHELL_EXTENSIONS_ARRAY[0]}'"
for extension in "${ENABLED_SHELL_EXTENSIONS_ARRAY[@]:1}"
do
     ENABLED_SHELL_EXTENSIONS+=", '$extension'"
done
ENABLED_SHELL_EXTENSIONS+="]"
echo $ENABLED_SHELL_EXTENSIONS


#blender `#3D Software Powerhouse` \
#calibre `#Ebook management` \
#darktable `#Easy RAW Editor` \
#evolution-spamassassin `#Helps you deal with spam in Evolution` \
#filezilla `#S/FTP Access` \
#gimp-data-extras \
#gimp-dbp \
#gimp-dds-plugin \
#gimp-elsamuko \
#gimp-focusblur-plugin \
#gimp-fourier-plugin \
#gimpfx-foundry.noarch \
#gimp-high-pass-filter \
#gimp-layer-via-copy-cut \
#gimp-lensfun \
#gimp-lqr-plugin \
#gimp-luminosity-masks \
#gimp-paint-studio \
#gimp-resynthesizer \
#gimp-save-for-web \
#gimp-wavelet-decompose \
#gimp-wavelet-denoise-plugin \
#glances `#Nice Monitor for your System` \
#gmic-gimp \
#gnome-shell-extension-dash-to-dock `#dash for gnome` \
#GREYCstoration-gimp \
#kdenlive  `#Advanced Video Editor` \
#krita  `#Painting done right` \
#mpv `#The best media player (with simple gui)` \
#mumble `#Talk with your friends` \
#nautilus-image-converter \
#nautilus-search-tool \
#openshot `#Simple Video Editor` \
#openssh-askpass `#Base Lib to let applications request ssh pass via gui` \
#pulseeffects `#Tweak your Music!` \
#python3-neovim `#Python Neovim Libs` \
#rawtherapee `#Professional RAW Editor` \
#spamassassin `#Dep to make sure it is locally installed for Evolution` \
#tilix `#The best terminal manager i know of` \
#tilix-nautilus `#Adds right click open in tilix to nautilus` \
#vagrant `#Virtual Machine management and autodeployment` \
#vagrant-libvirt `#integration with libvirt` \
#virt-manager `#A gui to manage virtual machines` \
#ansible `#Awesome to manage multiple machines or define states for systems` \
#adobe-source-code-pro-fonts `#The most beautiful monospace font around` \
#gitg `#a gui for git, a little slow on larger repos sadly` \
#tig `#cli git tool` \
#zsh `#Best shell` \
#zsh-syntax-highlighting `#Now with syntax highlighting` \
#cantata `#A beautiful mpd control` \
#caddy `#A quick webserver that can be used to share a directory with others in <10 seconds` \
#cockpit `#A An awesome local and remote management tool` \
#cockpit-bridge \
#fortune-mod `#Inspiring Quotes` \
#hexchat `#Irc Client` \
#libguestfs-tools `#Resize Vm Images and convert them` \
#ncdu `#Directory listing CLI tool. For a gui version take a look at "baobab"` \
#nextcloud-client `#Nextcloud Integration for Fedora` \
#nextcloud-client-nautilus `#Also for the File Manager, shows you file status` \
#sqlite-analyzer `#If you work with sqlite databases` \
#sqlitebrowser `#These two help alot`

# python packages
pip3 install --user matplotlib pandas #jupyter

###
# Install multimedia packages (codecs etc.)
###
# see https://rpmfusion.org/Configuration
sudo dnf groupupdate -y multimedia --setop="install_weak_deps=False" --exclude=PackageKit-gstreamer-plugin


###
# Setup automatic updates
###
sudo sed -i -E "s/^apply_updates = no/apply_updates = yes/g" /etc/dnf/automatic.conf
sudo sed -i -E "s/^download_updates = no/download_updates = yes/g" /etc/dnf/automatic.conf
# automically download security updates only (for all packages just keep using "default")
sudo sed -i -E "s/^upgrade_type = default/upgrade_type = security/g" /etc/dnf/automatic.conf
if [ "$NOSYSTEMD" = false ]; then
    # (source: https://linux-audit.com/automatic-security-updates-with-dnf/)
    sudo systemctl enable --now dnf-automatic.timer
fi


###
# Setup ClamAV (antivirus scanner)
###
# (sources: https://www.ctrl.blog/entry/how-to-periodic-clamav-scan.html,
# https://www.clamav.net/documents/installing-clamav-on-unix-linux-macos-from-source#first-time-set-up,
# https://srvfail.com/install-clamav-on-rhelcentos-7-and-configure-clamd/)
if [ "$CLAMAV" = true ]; then
    sudo dnf install -y clamav clamd clamav-update
    if [ $(getenforce) = 'Enforcing' ] || [ $(getenforce) = 'Permissive' ]; then
        # set the SELinux boolean for giving the ClamAV engine unrestricted access
        # to the system
        # (see https://www.clamav.net/documents/installation-on-redhat-and-centos-linux-distributions#configure-selinux-for-clamav)
        sudo setsebool -P antivirus_can_scan_system 1
    fi
    # configure freshclam
    sudo sed -i '/^Example$/d' /etc/freshclam.conf  # remove line with example to validate config
    sudo sed -i -E "s/^#[[:space:]]*SafeBrowsing [[:alnum:]]+/SafeBrowsing yes/g" /etc/freshclam.conf
    sudo sed -i -E "s/^#[[:space:]]*Bytecode [[:alnum:]]+/Bytecode yes/g" /etc/freshclam.conf
    sudo freshclam
    # configure clamd (daemon)
    sudo sed -i '/^Example$/d' /etc/clamd.d/scan.conf  # remove line with example to validate config
    sudo sed -i -E "s/^#[[:space:]]*LocalSocket /LocalSocket /g" /etc/clamd.d/scan.conf
    # Stop clamd when out of memory
    sudo sed -i -E "s/^#[[:space:]]*ExitOnOOM /ExitOnOOM /g" /etc/clamd.d/scan.conf
    if [ "$NOSYSTEMD" = false ]; then
        # keep used system ressources low
        sudo cp /usr/lib/systemd/system/clamd@.service /etc/systemd/system/clamd@.service
        sudo sed -i 's/^\[Service\]/\[Service\]\
Nice=18\
IOSchedulingClass=idle\
CPUSchedulingPolicy=idle\
/g' /etc/systemd/system/clamd@.service
        sudo systemctl daemon-reload
        sudo systemctl start clamd@scan
        # sudo systemctl status clamd@scan
        sudo systemctl enable clamd@scan
        # get current cronjobs (as string) which can fail if no cronjobs are initialized before (but
        # that is OK, therefore we disable return error check for this line using `|| true`)
        sudo crontab -l 2>/dev/null > crontab.temp || true
        # configure regular ClamAV updates (every 8 hours), scans of /var/www/ everyday and full scans (/) every month (first Sunday)
        t="0 */8  *  *  0  nice -n 16  systemd-cat --identifier=\"clamav-update\" /usr/bin/freshclam
0 5 * * 0 nice -n 16  systemd-cat --identifier=\"clamav-scan\" clamdscan --quiet --fdpass /var/www
30 5 * * 7 nice -n 18  systemd-cat --identifier=\"clamav-scan\" clamdscan --quiet --fdpass /"
        idempotent_file_append crontab.temp "$t"
        # load file as new crontab file
        sudo crontab crontab.temp
        # cleanup
        rm crontab.temp
    fi
fi


if [ "$LAPTOP" = true ]; then
    sudo dnf install -y \
    tlp `#Linux Advanced Power Management` \
    tlp-rdw `#Radio Device Wizard`

    # This is required to accomodate for the following warning:
    # `Warning: systemd-rfkill.socket is not masked, radio device switching may not work as configured.`
    sudo systemctl mask systemd-rfkill.socket

    # Enable TLP (TLP will start automatically on boot, but this avoids a restart)
    sudo tlp start

    if [ "$THINKPAD" = true ]; then
        ###
        # Thinkpad Extras Repo
        # This is holding the packages of the TLP application (power-management for Thinkpads)
        ###

        sudo dnf install -y https://repo.linrunner.de/fedora/tlp/repos/releases/tlp-release.fc$(rpm -E %fedora).noarch.rpm

        # the following package (akmod-acpi_call) provide battery charge thresholds and recalibration for Thinkpads
        sudo dnf install -y kernel-devel  # Needed for the akmod package below
        sudo dnf install -y akmod-acpi_call
        # If a Thinkpad older than x220: instead of akmod-acpi_call the following is used
        # sudo dnf install -y akmod-tp_smapi
    fi
fi

# TODO Pulseaudio got replaced with pipewire => Does Pipewire have an echo cancel feature too?
# see https://gitlab.freedesktop.org/pipewire/pipewire/-/issues/1375
# /usr/share/pipewire/pipewire.conf
# '
# context.modules = [
#     ...                 <-- This must include the original contents!!!
#     {   name = libpipewire-module-echo-cancel
#         args = {
#             source.props = {
#                 node.name = "echo-cancel.source"
#                 node.description = "Echo Cancelled Mic"
#             }
#             sink.props = {
#                 node.name = "echo-cancel.sink"
#                 node.description = "Echo Cancelled Output"
#             }
#         }
#     }
# ]
# '


# Add echo cancellation (as device in in/out-device-list in audio)
# FIXME This could pose a problem with linewise appending if the line `.ifexists module-echo-cancel.so` has already been used somewhere else
#t="
### Add echo cancellation device
#.ifexists module-echo-cancel.so
#load-module module-echo-cancel
#.endif
#"
#idempotent_file_append /etc/pulse/default.pa "$t"


###
# Remove some un-needed stuff
###

sudo dnf remove \
-y \
gnome-shell-extension-background-logo `#Tasteful but nah` \
chromium `#Using Chromium resets chromium-vaapi so remove it if installed, userprofiles will be kept and can be used in -vaapi`
#totem `#With mpv installed totem became a little useless` \


# This flag is needed for example for a CI/CD pipeline since Docker images do not use
# systemd (and especially do not start it as init system with PID 1)
if [ "$NOSYSTEMD" = false ]; then

    # Set tuning power profile
    sudo systemctl enable --now tuned
    if [ "$LAPTOP" = true ]; then
        # Balanced
        sudo tuned-adm profile balanced
        # Battery Saving:
        #sudo tuned-adm profile powersave
    else
        # Performance
        sudo tuned-adm profile desktop
    fi
fi


###
# Theming and GNOME Options
#
# Search for settings with e. g. `gsettings list-recursively | grep gedit` to find all
# settings for gedit. With e. g. `gsettings range org.gnome.gedit.preferences.editor tabs-size`
# the range of possible settings can be determined
###

# Tilix Dark Theme
#gsettings set com.gexperts.Tilix.Settings theme-variant 'dark'

# Gnome Shell Theming
gsettings set org.gnome.desktop.interface gtk-theme 'Arc-Dark-solid' # Dark theme without transparency
gsettings set org.gnome.desktop.interface cursor-theme 'Breeze_Snow'
gsettings set org.gnome.desktop.interface icon-theme 'Papirus-Dark'
gsettings set org.gnome.shell.extensions.user-theme name 'Arc-Dark-solid'

# Favourite Apps (bar at the bottom edge)
gsettings set org.gnome.shell favorite-apps "[\
'firefox.desktop', \
'chromium-freeworld.desktop', \
'torbrowser.desktop', \
'org.gnome.Nautilus.desktop', \
'org.gnome.gedit.desktop', \
'com.jetbrains.IntelliJ-IDEA-Community.desktop', \
'codium.desktop', \
'telegramdesktop.desktop', \
'discord_discord.desktop', \
'org.keepassxc.KeePassXC.desktop', \
'gnome-system-monitor.desktop', \
'com.gexperts.Tilix.desktop' \
]"

#Set SCP as Monospace (Code) Font
#gsettings set org.gnome.desktop.interface monospace-font-name 'Source Code Pro Semi-Bold 12'

#Set Extensions for gnome
gsettings set org.gnome.shell enabled-extensions "$ENABLED_SHELL_EXTENSIONS"

#Better Font Smoothing
#gsettings set org.gnome.settings-daemon.plugins.xsettings antialiasing 'rgba'

#Usability Improvements
gsettings set org.gnome.desktop.peripherals.touchpad tap-to-click true
gsettings set org.gnome.desktop.peripherals.touchpad two-finger-scrolling-enabled true
gsettings set org.gnome.desktop.peripherals.touchpad disable-while-typing true  # Set false for gaming
gsettings set org.gnome.desktop.peripherals.mouse accel-profile 'adaptive'
gsettings set org.gnome.desktop.sound allow-volume-above-100-percent true
gsettings set org.gnome.desktop.calendar show-weekdate true
gsettings set org.gnome.desktop.wm.preferences resize-with-right-button true
gsettings set org.gnome.desktop.wm.preferences button-layout 'appmenu:minimize,maximize,close'
gsettings set org.gnome.shell.overrides workspaces-only-on-primary false


if [ "$LAPTOP" = true ]; then
    # This indexer is nice, but can be detrimental for laptop users battery life
    # if the old tracker or the new version 3 (Fedora >33) is used in the System
    IS_NEW_TRACKER=$(echo $(gsettings writable org.freedesktop.Tracker3.Miner.Files index-on-battery))
    # the old tracker has no version number
    TRACKER_VERSION=""
    if [ "$IS_NEW_TRACKER" == 'true' ]; then
        TRACKER_VERSION="3"
    fi
    gsettings set org.freedesktop.Tracker"$TRACKER_VERSION".Miner.Files index-on-battery false
    gsettings set org.freedesktop.Tracker"$TRACKER_VERSION".Miner.Files index-on-battery-first-time false
    gsettings set org.freedesktop.Tracker"$TRACKER_VERSION".Miner.Files throttle 15
fi

#Nautilus (File Manager) Usability
gsettings set org.gnome.nautilus.icon-view default-zoom-level 'standard'
gsettings set org.gnome.nautilus.preferences default-folder-viewer 'list-view'
#gsettings set org.gnome.nautilus.preferences executable-text-activation 'display'
gsettings set org.gtk.Settings.FileChooser sort-directories-first true
gsettings set org.gnome.nautilus.list-view use-tree-view true
gsettings set org.gnome.nautilus.list-view default-zoom-level 'small'

#Gnome Night Light (Like flux/redshift)
gsettings set org.gnome.settings-daemon.plugins.color night-light-enabled true
gsettings set org.gnome.settings-daemon.plugins.color night-light-schedule-automatic false
gsettings set org.gnome.settings-daemon.plugins.color night-light-schedule-to 9.0
gsettings set org.gnome.settings-daemon.plugins.color night-light-schedule-from 18.0
gsettings set org.gnome.settings-daemon.plugins.color night-light-temperature 2700

# Deactivate Animations
gsettings set org.gnome.desktop.interface enable-animations false

# Gedit (Texteditor) Usability
gsettings set org.gnome.gedit.preferences.editor display-line-numbers true
gsettings set org.gnome.gedit.preferences.editor tabs-size 4
gsettings set org.gnome.gedit.preferences.editor insert-spaces true
gsettings set org.gnome.gedit.preferences.editor scheme 'oblivion'
gsettings set org.gnome.gedit.preferences.editor display-right-margin true
gsettings set org.gnome.gedit.preferences.editor right-margin-position 79
gsettings set org.gnome.gedit.preferences.editor highlight-current-line false
gsettings set org.gnome.gedit.preferences.editor display-line-numbers true
gsettings set org.gnome.gedit.preferences.editor bracket-matching true
gsettings set org.gnome.gedit.preferences.editor syntax-highlighting true
gsettings set org.gnome.gedit.preferences.ui side-panel-visible false
gsettings set org.gnome.gedit.preferences.ui statusbar-visible true
gsettings set org.gnome.gedit.plugins active-plugins "['git', 'spell', 'codecomment', 'time', 'textsize', 'wordcompletion', 'filebrowser', 'multiedit', 'modelines', 'docinfo', 'sort']"
gsettings set org.gnome.gedit.plugins.wordcompletion interactive-completion true
gsettings set org.gnome.gedit.plugins.wordcompletion minimum-word-size 2

# Deactivate smoothing for pictures when zooming in so pixel art looks crisp
gsettings set org.gnome.eog.view extrapolate false

# Use Capslock as another Ctrl-Key
gsettings set org.gnome.desktop.input-sources xkb-options "['caps:ctrl_modifier']"
# Use Capslock as another Esc-Key
#gsettings set org.gnome.desktop.input-sources xkb-options "['caps:escape']"
# to reset to default
#gsettings reset org.gnome.desktop.input-sources xkb-options
# alternatively the gnome-tweak-tool can be used to set it manually:
# "Keyboard & Mouse" > "Additional Layout Options" > "Ctrl position" > "Swap Ctrl and Caps Lock."

# Set Vim as the default git editor (instead of nano)
git config --global core.editor vim

# Set Java 8 as the default for Android programming
sudo alternatives --set java java-1.8.0-openjdk.x86_64

# Do not enable bluetooth on boot (to reduce power draw if it is rarely used)
# (see https://wiki.archlinux.org/title/Bluetooth#Configuration)
sudo sed -i -E "s/^AutoEnable=true/AutoEnable=false/g" /etc/bluetooth/main.conf

# Configure Android SDK
# if adb exists it seems like it is already downloaded correctly
if [ ! -f android-sdk/platform-tools/adb ]; then
    ANDROID_STUDIO_WEBSITE="$(wget -N -qO- https://developer.android.com/studio)"
    ANDROID_TOOLS="$(echo $ANDROID_STUDIO_WEBSITE | grep -Eo 'https://dl.google.com/android/repository/commandlinetools-linux-[0-9]+_latest.zip')"
    # download the android tools if it has not already happened
    if [ ! -f android_tools.zip ]; then
        curl $ANDROID_TOOLS > android_tools.zip
    fi
    #TODO ANDROID_TOOLS_CHECKSUM="$(echo $ANDROID_STUDIO_WEBSITE | grep -Eo 'https://dl.google.com/android/repository/commandlinetools-linux-[0-9]+_latest.zip')"
    #TODO checksum verification
    mkdir -p ~/android-sdk
    unzip -ud ~/android-sdk android_tools.zip
    #FIXME rm android_tools.zip
fi
# write env variables to .bash_profile to make it also available after restart
# TODO ANDROID_HOME is deprecated! Do we still need it? (see https://developer.android.com/studio/command-line/variables#envar)
t="
# Android SDK configuration:
export ANDROID_HOME=\$HOME/android-sdk
export ANDROID_SDK_ROOT=\$ANDROID_HOME
export PATH=\$PATH:\$HOME/android-sdk:\$HOME/android-sdk/tools
# For SDK version r_08 and higher, also add this for adb:
export PATH=\$PATH:\$HOME/android-sdk/platform-tools
"
idempotent_file_append ~/.bash_profile "$t"
# FIXME manually add the Env-Variables since sourcing the .bashrc makes problems
# TODO ANDROID_HOME is deprecated! Do we still need it? (see https://developer.android.com/studio/command-line/variables#envar)
export ANDROID_HOME=$HOME/android-sdk
export ANDROID_SDK_ROOT=$ANDROID_HOME
export PATH=$PATH:$HOME/android-sdk/platform-tools/
# temporarily disable checking for EPIPE error and use yes to accept all licenses
set +o pipefail
# '--sdk_root=..' because of a problem fixed here:
# https://stackoverflow.com/questions/60730615/sdkmanager-warning-could-not-create-setting-java-lang-illegalargumentexcepti/60731814#60731814
# accept all licenses
# TODO Consider this: https://stackoverflow.com/questions/60440509/android-command-line-tools-sdkmanager-always-shows-warning-could-not-create-se/61176718#61176718
yes | ~/android-sdk/*/bin/sdkmanager --sdk_root=$ANDROID_HOME --licenses
# install android-platform-tools
yes | ~/android-sdk/*/bin/sdkmanager --sdk_root=$ANDROID_HOME "platform-tools"
# download a version of the android sdk
yes | ~/android-sdk/*/bin/sdkmanager --sdk_root=$ANDROID_HOME "platforms;android-26"
set -uo pipefail
sudo dnf install -y glibc.i686 glibc-devel.i686 libstdc++.i686 zlib-devel.i686 ncurses-devel.i686 libX11-devel.i686 libXrender.i686 libXrandr.i686

# Flatpak & Flathub installation
sudo dnf install -y flatpak
flatpak remote-add --if-not-exists --user flathub https://flathub.org/repo/flathub.flatpakrepo

flatpak install --user flathub com.discordapp.Discord -y
flatpak install --user flathub com.jetbrains.PyCharm-Community -y
flatpak install --user flathub com.jetbrains.IntelliJ-IDEA-Community -y
#flatpak install --user flathub com.teamspeak.TeamSpeak -y

# Steam games (32bit) have issues with the too new 32bit compat libs in fedora
# Flatpak is the better option here
if [ "$STEAMFLAT" = true ]; then
    flatpak install --user flathub com.valvesoftware.Steam -y

    # Use steam with (hopefully) faster mesa beta drivers
    # (https://gitlab.com/freedesktop-sdk/freedesktop-sdk/-/wikis/Mesa-git)
    flatpak remote-add --user flathub-beta https://flathub.org/beta-repo/flathub-beta.flatpakrepo
    flatpak install --user flathub-beta \
        org.freedesktop.Platform.GL.mesa-git//21.08 \
        org.freedesktop.Platform.GL32.mesa-git//21.08

    flatpak update --user

    # To run it with mesa-git:
    #FLATPAK_GL_DRIVERS=mesa-git flatpak run com.valvesoftware.Steam
    # To always run with mesa-git (this may need to be redone after Steam updates):
    #sudo sed -i "s,Exec=,Exec=env FLATPAK_GL_DRIVERS=mesa-git ," /var/lib/flatpak/exports/share/applications/com.valvesoftware.Steam.desktop
	# Installed but not displayed? Check with: flatpak run com.valvesoftware.Steam

	# TODO set ` "SignIntoFriends"		"0"` in .var/app/com.valvesoftware.Steam/.local/share/Steam/userdata/65336895/config/localconfig.vdf (Flatpak version)
fi

##
# Install snap (since Discord from Flatpak can not screen-share)
##
# This is still just marginally better than on Flatpak but at least it is possible to share (seemingly) X11 Windows (like CLion)
sudo dnf install -y snapd
# fix for "too early for operation, device not yet seeded or device model not acknowledged" which
# seems to be caused by snap not yet having generated a seed value
# (source: https://bugs.launchpad.net/snapd/+bug/1826662/comments/7)
sudo snap wait system seed.loaded
sudo snap install discord
## install CLion
# to circumvent the error `error: cannot install "clion": classic confinement requires snaps under /snap or symlink from /snap to /var/lib/snapd/snap` just do:
sudo ln -s /var/lib/snapd/snap /snap
# actually install CLion
sudo snap install clion --classic


#The user needs to reboot to apply all changes.
echo "Please Reboot" && exit 0
