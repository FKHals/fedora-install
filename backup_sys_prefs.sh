#!/bin/bash
#########################################################################################
# Fedora (Linux OS) backup script
#
# This script backups certain data that is important to easily restore a working system
# based on the Fedora Linux distribution later on. The backup file gets stored in the
# $HOME directory.
# The directories in other linux distributions may differ for each purpose.
#
# Usage: `bash <scriptname>` or `./<scriptname>`
# (instead of `. <scriptname>` because otherwise the "strict"-mode would exit the script
# AND the terminal immediately on error)
#
# by Felix Korthals
# October 2020
#########################################################################################

# use an unofficial "strict-mode" so the script won't fail silently
set -uo pipefail
trap 's=$?; echo "$0: Error on line "$LINENO": $BASH_COMMAND"; exit $s' ERR
# set Internal Field Separtor to make word splittings only happen on newlines or tabs
# because the (normally present) whitespaces are "likely to cause surprising and
# confusing bugs" (see http://redsymbol.net/articles/unofficial-bash-strict-mode/)
IFS=$'\n\t'

BACKUPTIME="$(date +%d-%b-%Y)"
BACKUP_FULL_PATH="fedora_prefs_bak_$BACKUPTIME.tar.gz"

# save current working dir to go back at the end
PREVIOUS_WORKING_DIR=$PWD

# the script works relative to home to make the later extraction easier (because there
# is just root or home and root uses absolute paths anyway)
cd $HOME

#---------------------------------------------------------------------------------------#

# wifi preferences directory
WIFI="/etc/sysconfig/network-scripts"
# network (e. g. VPNs etc.) preferences directory
NETWORK="/etc/NetworkManager/system-connections"
# printer configurations dir.
PRINTERS="/etc/cups"

# whitespaces are not allowed for separation because of the modified IFS so newlines are
# used instead
ROOT_DIRS="
$WIFI
$NETWORK
$PRINTERS
"

ROOT_TAR_NAME="root.tar"

# create sub-package to separate where the file come from (in this case the root dir)
sudo tar -cpf $ROOT_TAR_NAME $ROOT_DIRS

#---------------------------------------------------------------------------------------#

# ssh keys directory
SSH=".ssh"
# gpg keys
GPG=".gnupg"
# git user config file
GIT_CFG=".gitconfig"
# firefox profile (bookmarks, etc.)
FIREFOX=".mozilla/firefox/*.default"

CONFIGS_DIR=".config"
# gnome latex build tools
GNOME_LATEX="$CONFIGS_DIR/gnome-latex/build_tools.xml"
# nautilus directory bookmarks
NAUTILUS_BOOKMARKS="$CONFIGS_DIR/gtk-3.0/bookmarks"
# sway configuration (xfce term just for usage with sway)
SWAY="
$CONFIGS_DIR/sway
$CONFIGS_DIR/waybar
$CONFIGS_DIR/xfce4/terminal/terminalrc
"

DISCORD="$CONFIGS_DIR/discord/settings.json"
TELEGRAM=".local/share/TelegramDesktop"
TERRARIA=".local/share/Terraria"
STEAM=".local/share/Steam/userdata/*/config/localconfig.vdf"

USER_DIRS="
$SSH
$GPG
$GIT_CFG
$FIREFOX
$GNOME_LATEX
$NAUTILUS_BOOKMARKS
$SWAY
$DISCORD
$TELEGRAM
$TERRARIA
$STEAM
"

USER_DIRS+="$STEAM"
USER_TAR_NAME="user.tar"

tar -cpf $USER_TAR_NAME $USER_DIRS

#---------------------------------------------------------------------------------------#

DIRS_2_BACKUP="
$ROOT_TAR_NAME
$USER_TAR_NAME
"

sudo tar -cpzf $BACKUP_FULL_PATH $DIRS_2_BACKUP 

#cleanup
rm -f $ROOT_TAR_NAME $USER_TAR_NAME

cd $PREVIOUS_WORKING_DIR

echo "Destination: $HOME/$BACKUP_FULL_PATH"
echo "Backup finished."
