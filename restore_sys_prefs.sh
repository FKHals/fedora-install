#!/bin/bash
#########################################################################################
# Fedora restore script
#
# This script restores backuped data from a System running Fedora (Linux OS). The backup
# file must be stored in the $HOME directory.
# The directories in other linux distributions may differ for each purpose.
#
# Usage: `bash <scriptname>` or `./<scriptname>`
# (instead of `. <scriptname>` because otherwise the "strict"-mode would exit the script
# AND the terminal immediately on error)
#
# by Felix Korthals
# October 2020
#########################################################################################

# use an unofficial "strict-mode" so the script won't fail silently
set -uo pipefail
trap 's=$?; echo "$0: Error on line "$LINENO": $BASH_COMMAND"; exit $s' ERR
# set Internal Field Separtor to make word splittings only happen on newlines or tabs
# because the (normally present) whitespaces are "likely to cause surprising and
# confusing bugs" (see http://redsymbol.net/articles/unofficial-bash-strict-mode/)
IFS=$'\n\t'

# save current working dir to go back at the end
PREVIOUS_WORKING_DIR=$PWD

# the script works relative to home to make extraction easier
cd $HOME

# Firefox New Profile #-----------------------------------------------------------------#

PROFILE_LOC="^.mozilla/firefox/[[:alnum:]]+\.default[^/]*"
# find the new firefox profile directory where the places.sqlite resides (the active one)
NEW_FIREFOX_PROFILE=$(find .mozilla/firefox/ -name "places.sqlite" | grep -oE $PROFILE_LOC)
echo $NEW_FIREFOX_PROFILE

#---------------------------------------------------------------------------------------#

# names of the intermediate files, created by the backup script (backup_sys_prefs.sh)
ROOT_TAR_NAME="root.tar"
USER_TAR_NAME="user.tar"

# find the newest backup file
RESTORE_FILE_PATH="$(ls -t ./fedora_prefs_bak_*.tar.gz | head -1)"

# extract gzipped file locally
tar -xzf $RESTORE_FILE_PATH

# extract system files to root
sudo tar -xf $ROOT_TAR_NAME -C /

# extract user files locally
tar -xf $USER_TAR_NAME

# Firefox #-----------------------------------------------------------------------------#

# find the old firefox profile directory inside user.tar
OLD_FIREFOX_PROFILE=$(tar -tf user.tar | grep -E "$PROFILE_LOC/places\.sqlite" | grep -oE $PROFILE_LOC | tail -1)
# move the content of the old profile directory into the new directory (preserving
# everything including symlinks by using '-a' and the '.' also includes hidden files)
cp -a "$OLD_FIREFOX_PROFILE/." "$NEW_FIREFOX_PROFILE/"
rm -r $OLD_FIREFOX_PROFILE

# Network/VPN #------------------------------------------------------------------------#

# Since VPN configs include usernames: Replace old username with new username (double
# quoted regex is needed for variable usage)
cd /etc/NetworkManager/system-connections
sudo sed -i -e "s/user\:[^\:]*\:/user:$(whoami):/" *
systemctl restart NetworkManager
cd $HOME

#---------------------------------------------------------------------------------------#

# cleanup
rm -f $ROOT_TAR_NAME $USER_TAR_NAME

cd $PREVIOUS_WORKING_DIR
